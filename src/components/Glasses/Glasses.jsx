import React, { Component } from 'react';
import './Glasses.css';
import dataGlasses, { glassesArr } from './dataGlasses';

export default class Glasses extends Component {
  state = {
    changeGlasses: glassesArr[0],
  };
  handleChangeGlasses = (changeColor) => {
    this.setState({
      changeGlasses: changeColor,
    });
  };
  renderGlassesList = () => {
    let glassesList = glassesArr.map((item) => {
      console.log('item: ', item);
      return (
        <div className="card col-2" style={{ width: '18rem' }}>
          <img src={item.url} className="card-img-top" alt="..." />
          <button
            onClick={() => {
              this.handleChangeGlasses(item);
            }}
            className="btn btn-primary mt-2">
            Try It On
          </button>
        </div>
      );
    });
    return glassesList;
  };

  render() {
    return (
      <div className="row container mx-auto  ">
        <div className="col-0 pt-5">
          {/* glass image  */}
          <div className="tryGlasses ">
            <img
              style={{ width: '70%' }}
              src={this.state.changeGlasses.url}
              alt=""
            />
          </div>

          {/* model  */}
          <div className=" model">
            <img
              className="pl-5 pb-5"
              src="./baitap_glasses/glassesImage/model.jpg"
              alt=""
            />
            <img
              className="pl-5 pb-5"
              src="./baitap_glasses/glassesImage/model.jpg"
              alt=""
            />
          </div>
          <div className="bg-info  p-2 desc">
            <h5 className="text-warning">{this.state.changeGlasses.name}</h5>
            <p className="text-black-50">{this.state.changeGlasses.desc}</p>
          </div>
        </div>
        <div className="row container mx-auto">{this.renderGlassesList()}</div>
      </div>
    );
  }
}
